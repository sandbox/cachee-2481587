CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Using 'Validate Data'
 * Troubleshooting
 * FAQ
 * Maintainers
 * Design Decisions


INTRODUCTION
------------
The 'Validate Data' module allows a developer to add a validation test function
to any input field available on a node or web form. We call these entities 
or functions "validators". This module helps you by building the java and php 
code needed to test the input data after it is submitted via a "mouse click out" 
or "tabbing out" of the input field (if JAVA is enabled). A message is 
display immediately if the data did not meet the criteria. Additional, 
if the JAVA is available the save button will be disabled until the faulty
fields have been corrected. When the module is enabled a directory 
(/validate_data) and pair of files are created in the sites "Public file 
system path". If that directory does not contain a "files" directory a 
warning message is displayed.

You have three steps to complete in order for the validation to take place. 
Part one is the JAVA code which is how the data is verified when entered. 
You will create these validators via the Jquery Validators page ->
admin/structure/validate_data/jqueryvalidators

Part two is the PHP code which is used to insure that the bad data is not 
stored on the database. You will create these validators via the PHP 
Validators page -> admin/structure/validate_data/phpvalidators. An additional
reason for the PHP is to insure the error is caught if the user has JAVA
disabled. This function is called when the node or webform is saved.

Part three is to connect the different validators to a field on a node or 
webform. You will connect them via the Field Validations page by selecting
a node (content) type, a field from that page and one or more validators 
from the Jquery and PHP list. You will create these validations via the 
Field Validations page -> admin/structure/validate_data/fieldvalidations.

The main reason there are three parts is that you now can reuse the JAVA and 
PHP code on multiple fields. So on the JQuery and PHP pages you will define 
what you are testing. For example the data inputted must be numeric, so you 
would create at JQuery and PHP validator Isnotnumeric and select error, the 
"not" and "isnumeric" for the test conditions, then add an error message 
"The value must be numeric". You can optionally defining a background color,
padding, left margin and border. 

Other tools available are maintenance type which allow you edit, delete or
display existing validators or field validation entries or you can export
and import the validate_data files. 

To recap following are the tools to build the first three parts:
 * Create a JQuery validator
 * Create a PHP validator
 * Connect the JQuery validator and PHP validator to a particular field

And following are the maintenance functions:
 * Update or Deletion of the JQuery or PHP validators
 * Update or Deletion the field validation
 * Export or Import of the validate_data files
 * Display of the JQuery or PHP code 
 
 
REQUIREMENTS
------------
This module requires no other modules


INSTALLATION
------------
* Install as you would normally install a contributed Drupal module. See:
  https://drupal.org/documentation/install/modules-themes/modules-7 for 
  further information. 

* Insure that the files directory is available in the sites "Public file 
  system path" directory.

* Enable the module and then add the field validation via Structure -> Validate data.

CONFIGURATION
-------------
You can learn more about the functions of the 'Validate Data' module by selecting
the "Config" link on the module page or access it via config/validatedatasmenu.
This will display a menu along with a short description of the each of the 
available functions. Following is the list of the 'Validate Data' functions:

* JQuery Validator Descriptions establish the jquery validators you would 
  do on any field. These validators will warn users of errors real time.
* PHP Validator Descriptions establish the PHP validators you would do on 
  any field. These validators will happen on submit of the form and may stop
  the node from being saved.
* Field Validation links the fields of nodes or forms to a JQuery and/or PHP Validator
  validations to apply to that Field.
* Update or Delete Field validations on node or form fields. 
* Update or Delete PHP validator definition.
* Update or Delete JQUERY validator definition.
* Export Validate data will export all field validations and validator data
* Import Validate data will delete all old validation data and import the new data. 
* Display Jquery Code will display the Jquery file that is created.
* Display PHP Code will display the PHP file that is created.


USING VALIDATIONS
-----------------
To use the Validations module you will need to create:
   1. A JQuery Validator which is used to test the value entered into 
      the field. This is accomplished using the form via the 
      admin/structure/validate_data/jqueryvalidators.
   2. A PHP Validator which is used to test the value on the save and 
      insure back data is not stored on the database. This is accomplished
	  using the form via the admin/structure/validate_data/phpvalidators.
   3. A Field Validation which is used to link the two validators to a
      specific field on a node or webform. This is accomplished using 
	  the form via the admin/structure/validate_data/fieldvalidations. 

Once these steps are completed you can go to the node or webform and
check out the validation of the input data.

The maintenance of the site's data is as easy as creating the validations.
If you need to modify the message or test values use the appropriate 
maintenance page to access the validators. You need to add or remove a
validator from a field using the Field Validation Maintenance page -> 
admin/structure/validate_data/maintainfieldvalidations

TROUBLESHOOTING
---------------
 * If the field validation is not occurring, do the following:

   - Clear cache again

   - Make sure the correct field was selected in the "Field Validation"
   
   - Make sure that the correct "validations" files are available in the
     user's directory.
	 
FAQ
---
Q: 


A: 

MAINTAINERS
-----------
Current maintainers:
 * Karan Garske (karangarske) - https://drupal.org/u/karangarske
 * Caroline Achee (cachee) - https://drupal.org/u/cachee

This project has been sponsored by:
 * CK Creations
   Specialized in planning and creating Drupal powered sites, CK Creations offers 
   installation, development, theming, customization, and hosting to get you started.
   Visit https://www.ck-creations.com for more information.
